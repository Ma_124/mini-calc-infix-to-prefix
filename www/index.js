import * as wasm from "infix-to-prefix";

window.update = () => {
  let input = document.getElementById("input").value;

  let output_el = document.getElementById("output");

  output_el.innerText = input == "" ? "" : wasm.format(input);
};

try {
  update()
} catch (e) {}
