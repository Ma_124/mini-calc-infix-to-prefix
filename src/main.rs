use std::env;

use infix_to_prefix::infix_to_prefix;

fn main() {
    println!(
        "{}",
        infix_to_prefix(
            &env::args()
                .nth(1)
                .expect("usage: infix-to-prefix <formula>"),
        )
    )
}
