use wasm_bindgen::prelude::*;

use crate::{ascii_to_unicode, parse_formula};

#[wasm_bindgen]
pub fn format(infix: &str) -> String {
    match parse_formula(infix) {
        Ok(f) => format!("# \"{}\"\n\n{f}", ascii_to_unicode(infix)),
        Err(e) => e.to_string(),
    }
}
