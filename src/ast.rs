use std::fmt::{self, Display, Formatter};

#[derive(Debug, Clone, Copy)]
pub enum BinOp {
    Imp,
    Dis,
    Con,
}

#[derive(Debug, Clone, Copy)]
pub enum UnOp {
    Neg,
    Exi,
    Uni,
}

#[derive(Debug, Clone)]
pub enum Formula {
    Binary {
        lhs: Box<Self>,
        op: BinOp,
        rhs: Box<Self>,
    },
    Unary {
        op: UnOp,
        rhs: Box<Self>,
    },
    Atom(String),
}

impl Display for Formula {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Formula::Atom(s) = self {
            return write!(f, "{s}");
        }

        if f.alternate() {
            write!(f, "(")?;
        }

        match self {
            Formula::Binary { lhs, op, rhs } => write!(f, "{op:?} {lhs:#} {rhs:#}"),
            Formula::Unary { op, rhs } => write!(f, "{op:?} {rhs:#}"),
            _ => unreachable!(),
        }?;

        if f.alternate() {
            write!(f, ")")?;
        }
        Ok(())
    }
}
