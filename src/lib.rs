#![allow(clippy::result_large_err)]

use ast::{BinOp, Formula, UnOp};
use pest::{
    iterators::Pairs,
    pratt_parser::{Assoc, Op, PrattParser},
    Parser,
};
use pest_derive::Parser;

pub mod ast;

#[cfg(feature = "wasm")]
pub mod wasm;

#[derive(Parser)]
#[grammar = "infix.pest"]
struct InfixParser;

fn parser() -> PrattParser<Rule> {
    use Assoc::*;
    use Rule::*;

    PrattParser::new()
        .op(Op::prefix(uni) | Op::prefix(exi))
        .op(Op::infix(imply, Right))
        .op(Op::infix(or, Left))
        .op(Op::infix(and, Left))
        .op(Op::prefix(not))
}

fn pratt_formula(parser: &PrattParser<Rule>, pairs: Pairs<Rule>) -> Formula {
    parser
        .map_primary(|primary| match primary.as_rule() {
            Rule::atom => Formula::Atom(primary.as_str().to_owned()),
            Rule::expr => pratt_formula(parser, primary.into_inner()),
            _ => unreachable!(),
        })
        .map_prefix(|op, rhs| Formula::Unary {
            op: match op.as_rule() {
                Rule::not => UnOp::Neg,
                Rule::exi => UnOp::Exi,
                Rule::uni => UnOp::Uni,
                _ => unreachable!(),
            },
            rhs: Box::new(rhs),
        })
        .map_infix(|lhs, op, rhs| Formula::Binary {
            lhs: Box::new(lhs),
            op: match op.as_rule() {
                Rule::imply => BinOp::Imp,
                Rule::or => BinOp::Dis,
                Rule::and => BinOp::Con,
                _ => unreachable!(),
            },
            rhs: Box::new(rhs),
        })
        .parse(pairs)
}

pub fn parse_formula(input: &str) -> Result<Formula, pest::error::Error<Rule>> {
    let pairs = InfixParser::parse(Rule::expr_prog, input)?
        .next()
        .unwrap()
        .into_inner();
    Ok(pratt_formula(&parser(), pairs))
}

pub fn infix_to_prefix(infix: &str) -> String {
    match parse_formula(infix) {
        Ok(f) => f.to_string(),
        Err(e) => e.to_string(),
    }
}

pub fn ascii_to_unicode(ascii: &str) -> String {
    ascii
        .replace('~', "¬")
        .replace("?A", "∀")
        .replace("?E", "∃")
        .replace('&', "∧")
        .replace('|', "∨")
        .replace('>', "→")
}
