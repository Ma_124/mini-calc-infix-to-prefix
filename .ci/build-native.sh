#!/usr/bin/env bash
set -euo pipefail

if apt-get --version > /dev/null; then
    apt-get update
    apt-get install -y gcc-mingw-w64-x86-64
else
    echo "It seems like your system doesn't use" '`apt-get`.'
    echo 'You need to install the equivalent of `gcc-mingw-w64-x86-64` yourself.'
fi

set -x
rustup target add x86_64-unknown-linux-gnu x86_64-pc-windows-gnu

cargo build --locked --release --target x86_64-unknown-linux-gnu
cargo build --locked --release --target x86_64-pc-windows-gnu
rm -rf bin
mkdir bin

mv target/x86_64-unknown-linux-gnu/release/infix-to-prefix bin/
mv target/x86_64-pc-windows-gnu/release/infix-to-prefix.exe bin/
strip --strip-all bin/infix-to-prefix
