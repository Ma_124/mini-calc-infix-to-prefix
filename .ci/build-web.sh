#!/usr/bin/env bash
set -euo pipefail

if apt-get --version > /dev/null; then
    apt-get update
    apt-get install -y npm
else
    echo "It seems like your system doesn't use" '`apt-get`.'
    echo 'You need to install `npm` yourself.'
fi

wasm_pack_url="https://github.com/rustwasm/wasm-pack/releases/download/v0.12.1/wasm-pack-v0.12.1-x86_64-unknown-linux-musl.tar.gz"
wasm_pack_sum="4317d1aea126870141db635947da4154dced2837aec9dedeb372b7641b4ea717  .ci/wasm-pack"

if [[ "$(sha256sum .ci/wasm-pack)" != "$wasm_pack_sum" ]]; then
   curl -fsSL "$wasm_pack_url" | tar -xvzf - --strip-components 1 -C .ci wasm-pack-v0.12.1-x86_64-unknown-linux-musl/wasm-pack
fi

set -x
rustup target add wasm32-unknown-unknown

rm -rf pkg
.ci/wasm-pack build -- --locked --target x86_64-unknown-linux-gnu --features wasm

cd www
npm ci
NODE_OPTIONS=--openssl-legacy-provider npm run build
