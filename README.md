# Mini Calc: Infix to Prefix
Convert an easier to read infix syntax to [MiniCalc]'s prefix syntax.

Try the [online version](https://ma_124.gitlab.io/mini-calc-infix-to-prefix/).

This program accepts both Unicode and ASCII inputs:
```
¬ a
~ a
  Neg a

a → b → c
a > b > c
  Imp a (Imp b c)

a ∧ b ∨ c → d
a & b | c > d
  Imp (Dis (Con a b) c) d

a ∨ b ∧ c → d
a | b & c > d
  Imp (Dis a (Con b c)) d

(∀  p[0]) → (∃  p[0])
(?A p[0]) > (?E p[0])
  Imp (Uni p[0]) (Exi p[0])
```

<-- TODO: [MiniCalc]: -->

## License
Copyright &copy; 2021 Ma_124
[License](./LICENSE)

